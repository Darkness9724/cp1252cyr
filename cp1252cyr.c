#include <stdio.h>
#include <stdlib.h>

void display_help() {
    printf("Usage: cp1252cyr [optional file]\n");
    printf("-h\t display this help text and exit.\n");
}

void convert(char* input_char, ssize_t read) {
    for (int i = 0; i < read-1; ++i) {
        if (input_char[i] < 0) {
            //А
            if (input_char[i] == -48 && input_char[i+1] == -112) {
                int ch = 0x41;
                printf("%c", ch);
            }
            //Б
            if (input_char[i] == -48 && input_char[i+1] == -111) {
                int ch[3] = {0xE2, 0x82, 0xAC};
                printf("%c%c%c", ch[0], ch[1], ch[2]);
            }
            //В
            if (input_char[i] == -48 && input_char[i+1] == -110) {
                int ch = 0x42;
                printf("%c", ch);
            }
            //Г
            if (input_char[i] == -48 && input_char[i+1] == -109) {
                int ch[3] = {0xE2, 0x80, 0x9A};
                printf("%c%c%c", ch[0], ch[1], ch[2]);
            }
            //Д
            if (input_char[i] == -48 && input_char[i+1] == -108) {
                int ch[2] = {0xC6, 0x92};
                printf("%c%c", ch[0], ch[1]);
            }
            //Е
            if (input_char[i] == -48 && input_char[i+1] == -107) {
                int ch = 0x45;
                printf("%c", ch);
            }
            //Ё
            if (input_char[i] == -48 && input_char[i+1] == -127) {
                int ch[2] = {0xC3, 0x8B};
                printf("%c%c", ch[0], ch[1]);
            }
            //Ж
            if (input_char[i] == -48 && input_char[i+1] == -106) {
                int ch[3] = {0xE2, 0x80, 0x9E};
                printf("%c%c%c", ch[0], ch[1], ch[2]);
            }
            //З
            if (input_char[i] == -48 && input_char[i+1] == -105) {
                int ch[3] = {0xE2, 0x80, 0xA6};
                printf("%c%c%c", ch[0], ch[1], ch[2]);
            }
            //И
            if (input_char[i] == -48 && input_char[i+1] == -104) {
                int ch[3] = {0xE2, 0x80, 0xA0};
                printf("%c%c%c", ch[0], ch[1], ch[2]);
            }
            //Й
            if (input_char[i] == -48 && input_char[i+1] == -103) {
                int ch[3] = {0xE2, 0x80, 0xA1};
                printf("%c%c%c", ch[0], ch[1], ch[2]);
            }
            //К
            if (input_char[i] == -48 && input_char[i+1] == -102) {
                int ch = 0x4B;
                printf("%c", ch);
            }
            //Л
            if (input_char[i] == -48 && input_char[i+1] == -101) {
                int ch[2] = {0xCB, 0x86};
                printf("%c%c", ch[0], ch[1]);
            }
            //М
            if (input_char[i] == -48 && input_char[i+1] == -100) {
                int ch = 0x4D;
                printf("%c", ch);
            }
            //Н
            if (input_char[i] == -48 && input_char[i+1] == -99) {
                int ch = 0x48;
                printf("%c", ch);
            }
            //О
            if (input_char[i] == -48 && input_char[i+1] == -98) {
                int ch = 0x4F;
                printf("%c", ch);
            }
            //П
            if (input_char[i] == -48 && input_char[i+1] == -97) {
                int ch[3] = {0xE2, 0x80, 0xB0};
                printf("%c%c%c", ch[0], ch[1], ch[2]);
            }
            //Р
            if (input_char[i] == -48 && input_char[i+1] == -96) {
                int ch = 0x50;
                printf("%c", ch);
            }
            //С
            if (input_char[i] == -48 && input_char[i+1] == -95) {
                int ch = 0x43;
                printf("%c", ch);
            }
            //Т
            if (input_char[i] == -48 && input_char[i+1] == -94) {
                int ch = 0x54;
                printf("%c", ch);
            }
            //У
            if (input_char[i] == -48 && input_char[i+1] == -93) {
                int ch[3] = {0xE2, 0x80, 0xB9};
                printf("%c%c%c", ch[0], ch[1], ch[2]);
            }
            //Ф
            if (input_char[i] == -48 && input_char[i+1] == -92) {
                int ch[3] = {0xE2, 0x80, 0x98};
                printf("%c%c%c", ch[0], ch[1], ch[2]);
            }
            //Х
            if (input_char[i] == -48 && input_char[i+1] == -91) {
                int ch = 0x58;
                printf("%c", ch);
            }
            //Ц
            if (input_char[i] == -48 && input_char[i+1] == -90) {
                int ch[3] = {0xE2, 0x80, 0x99};
                printf("%c%c%c", ch[0], ch[1], ch[2]);
            }
            //Ч
            if (input_char[i] == -48 && input_char[i+1] == -89) {
                int ch[3] = {0xE2, 0x80, 0x9C};
                printf("%c%c%c", ch[0], ch[1], ch[2]);
            }
            //Ш
            if (input_char[i] == -48 && input_char[i+1] == -88) {
                int ch[3] = {0xE2, 0x80, 0x9D};
                printf("%c%c%c", ch[0], ch[1], ch[2]);
            }
            //Щ
            if (input_char[i] == -48 && input_char[i+1] == -87) {
                int ch[3] = {0xE2, 0x80, 0xA2};
                printf("%c%c%c", ch[0], ch[1], ch[2]);
            }
            //Ъ
            if (input_char[i] == -48 && input_char[i+1] == -86) {
                int ch[3] = {0xE2, 0x80, 0x93};
                printf("%c%c%c", ch[0], ch[1], ch[2]);
            }
            //Ы
            if (input_char[i] == -48 && input_char[i+1] == -85) {
                int ch[3] = {0xE2, 0x80, 0x94};
                printf("%c%c%c", ch[0], ch[1], ch[2]);
            }
            //Ь
            if (input_char[i] == -48 && input_char[i+1] == -84) {
                int ch[2] = {0xCB, 0x9C};
                printf("%c%c", ch[0], ch[1]);
            }
            //Э
            if (input_char[i] == -48 && input_char[i+1] == -83) {
                int ch[3] = {0xE2, 0x84, 0xA2};
                printf("%c%c%c", ch[0], ch[1], ch[2]);
            }
            //Ю
            if (input_char[i] == -48 && input_char[i+1] == -82) {
                int ch[3] = {0xE2, 0x80, 0xBA};
                printf("%c%c%c", ch[0], ch[1], ch[2]);
            }
            //Я
            if (input_char[i] == -48 && input_char[i+1] == -81) {
                int ch[2] = {0xC3, 0x97};
                printf("%c%c", ch[0], ch[1]);
            }
            //а
            if (input_char[i] == -48 && input_char[i+1] == -80) {
                int ch = 0x61;
                printf("%c", ch);
            }
            //б
            if (input_char[i] == -48 && input_char[i+1] == -79) {
                int ch[2] = {0xC2, 0xA0};
                printf("%c%c", ch[0], ch[1]);
            }
            //в
            if (input_char[i] == -48 && input_char[i+1] == -78) {
                int ch[2] = {0xC2, 0xA2};
                printf("%c%c", ch[0], ch[1]);
            }
            //г
            if (input_char[i] == -48 && input_char[i+1] == -77) {
                int ch[2] = {0xC2, 0xA5};
                printf("%c%c", ch[0], ch[1]);
            }
            //д
            if (input_char[i] == -48 && input_char[i+1] == -76) {
                int ch[2] = {0xC2, 0xA6};
                printf("%c%c", ch[0], ch[1]);
            }
            //е
            if (input_char[i] == -48 && input_char[i+1] == -75) {
                int ch = 0x65;
                printf("%c", ch);
            }
            //ё
            if (input_char[i] == -47 && input_char[i+1] == -111) {
                int ch[2] = {0xC3, 0xAB};
                printf("%c%c", ch[0], ch[1]);
            }
            //ж
            if (input_char[i] == -48 && input_char[i+1] == -74) {
                int ch[2] = {0xC2, 0xA8};
                printf("%c%c", ch[0], ch[1]);
            }
            //з
            if (input_char[i] == -48 && input_char[i+1] == -73) {
                int ch[2] = {0xC2, 0xA9};
                printf("%c%c", ch[0], ch[1]);
            }
            //и
            if (input_char[i] == -48 && input_char[i+1] == -72) {
                int ch[2] = {0xC2, 0xAA};
                printf("%c%c", ch[0], ch[1]);
            }
            //й
            if (input_char[i] == -48 && input_char[i+1] == -71) {
                int ch[2] = {0xC2, 0xAB};
                printf("%c%c", ch[0], ch[1]);
            }
            //к
            if (input_char[i] == -48 && input_char[i+1] == -70) {
                int ch[2] = {0xC2, 0xAC};
                printf("%c%c", ch[0], ch[1]);
            }
            //л
            if (input_char[i] == -48 && input_char[i+1] == -69) {
                int ch[2] = {0xC2, 0xAE};
                printf("%c%c", ch[0], ch[1]);
            }
            //м
            if (input_char[i] == -48 && input_char[i+1] == -68) {
                int ch[2] = {0xC2, 0xAF};
                printf("%c%c", ch[0], ch[1]);
            }
            //н
            if (input_char[i] == -48 && input_char[i+1] == -67) {
                int ch[2] = {0xC2, 0xB0};
                printf("%c%c", ch[0], ch[1]);
            }
            //о
            if (input_char[i] == -48 && input_char[i+1] == -66) {
                int ch = 0x6F;
                printf("%c", ch);
            }
            //п
            if (input_char[i] == -48 && input_char[i+1] == -65) {
                int ch[2] = {0xC2, 0xB1};
                printf("%c%c", ch[0], ch[1]);
            }
            //р
            if (input_char[i] == -47 && input_char[i+1] == -128) {
                int ch = 0x70;
                printf("%c", ch);
            }
            //с
            if (input_char[i] == -47 && input_char[i+1] == -127) {
                int ch = 0x63;
                printf("%c", ch);
            }
            //т
            if (input_char[i] == -47 && input_char[i+1] == -126) {
                int ch[2] = {0xC2, 0xB2};
                printf("%c%c", ch[0], ch[1]);
            }
            //у
            if (input_char[i] == -47 && input_char[i+1] == -125) {
                int ch[2] = {0xC2, 0xB3};
                printf("%c%c", ch[0], ch[1]);
            }
            //ф
            if (input_char[i] == -47 && input_char[i+1] == -124) {
                int ch[2] = {0xC2, 0xB4};
                printf("%c%c", ch[0], ch[1]);
            }
            //х
            if (input_char[i] == -47 && input_char[i+1] == -123) {
                int ch = 0x78;
                printf("%c", ch);
            }
            //ц
            if (input_char[i] == -47 && input_char[i+1] == -122) {
                int ch[2] = {0xC2, 0xB5};
                printf("%c%c", ch[0], ch[1]);
            }
            //ч
            if (input_char[i] == -47 && input_char[i+1] == -121) {
                int ch[2] = {0xC2, 0xB6};
                printf("%c%c", ch[0], ch[1]);
            }
            //ш
            if (input_char[i] == -47 && input_char[i+1] == -120) {
                int ch[2] = {0xC2, 0xB7};
                printf("%c%c", ch[0], ch[1]);
            }
            //щ
            if (input_char[i] == -47 && input_char[i+1] == -119) {
                int ch[2] = {0xC2, 0xB8};
                printf("%c%c", ch[0], ch[1]);
            }
            //ъ
            if (input_char[i] == -47 && input_char[i+1] == -118) {
                int ch[2] = {0xC2, 0xB9};
                printf("%c%c", ch[0], ch[1]);
            }
            //ы
            if (input_char[i] == -47 && input_char[i+1] == -117) {
                int ch[2] = {0xC2, 0xBA};
                printf("%c%c", ch[0], ch[1]);
            }
            //ь
            if (input_char[i] == -47 && input_char[i+1] == -116) {
                int ch[2] = {0xC2, 0xBB};
                printf("%c%c", ch[0], ch[1]);
            }
            //э
            if (input_char[i] == -47 && input_char[i+1] == -115) {
                int ch[2] = {0xC2, 0xBC};
                printf("%c%c", ch[0], ch[1]);
            }
            //ю
            if (input_char[i] == -47 && input_char[i+1] == -114) {
                int ch[2] = {0xC2, 0xBE};
                printf("%c%c", ch[0], ch[1]);
            }
            //я
            if (input_char[i] == -47 && input_char[i+1] == -113) {
                int ch[2] = {0xC3, 0xB7};
                printf("%c%c", ch[0], ch[1]);
            }
        } else {
            //ASCII
            printf("%c", input_char[i]);
        }
    }
    putchar(0xA);
}

int main(int argc, char** argv) {
    char* input_char = NULL; size_t len; ssize_t read;
    if (argc == 1) {
        while((read = getline(&input_char, &len, stdin)) != EOF)
            convert(input_char, read);
    } else {
        if ((*argv[1]) == '-') {
            const char *p=argv[1]+1;

            while ((*p)!='\0') {
                char c=*(p++);
                if ((c=='h') || (c=='H'))
                    display_help();
                }
            return 0;
        } else {
            FILE* file = fopen(argv[1], "rb"); 
            while((read = getline(&input_char, &len, file)) != EOF)
                convert(input_char, read);
        }
    }
    free(input_char);
    return 0;
}
